/*
  import modules
/*---------------------------------------*/
const gulp = require('gulp')
const plumber = require('gulp-plumber')
//- task: webpack
const webpack = require('webpack')
const webpackstream = require('webpack-stream')
const TerserPlugin = require('terser-webpack-plugin')
//- task: sass
const sass = require('gulp-sass')
const postcss = require('gulp-postcss')
const autoprefixer = require('autoprefixer')
const mqpacker = require('css-mqpacker')
//- task: watch
const watch = require('gulp-watch')
//- task: pug
const pug = require('gulp-pug')
// reneme
const rename = require('gulp-rename')



/*
  config
/*---------------------------------------*/
const dest = './dest'
const src = './src'
const path = require('path')
const relativeSrcPath = path.relative('.', src)
const Dotenv = require('dotenv-webpack')
// const VueLoaderPlugin = require('vue-loader/lib/plugin')
//- vueでpugを利用する時
//- npm install -D pug pug-plain-loader
//- @doc https://vue-loader.vuejs.org/guide/pre-processors.html#typescript
//- vue-template-compilerもインストールすること
//- npm i -D vue-template-compiler
//- npm i -D core-js3でcore-js3を読み込むこと babel7.4-からpolyfill禁止のため


/*
  config define
/*---------------------------------------*/
const config = {

  /*
    config: common
  /*---------------------------------------*/
  dest: dest,

  /*
    config: js
  /*---------------------------------------*/
  js: {
    src: [
      src + '/js/app.js',
    ],
    dest: dest + '/js',
  },

  /*
    config: webpack
  /*---------------------------------------*/
  webpack: {
    mode: 'development',
    entry: {
      app: [src + '/js/app.js'],
    },
    output: {
      filename: '[name].bundle.js'
    },
    resolve: {
      extensions: ['.js'],
      modules: ['node_modules'],
      alias: {
        // 'masonry': 'masonry-layout',
        // 'isotope': 'isotope-layout',
        // 'lightbox': 'lightbox2',
        'vue$': 'vue/dist/vue.common.js',
      }
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader",
            options: {
              babelrc: true
            }
            // options: {
            //  presets: ['@babel/preset-env'],
            //  plugins: [require('@babel/plugin-proposal-object-rest-spread')]
            // }
          }
        },
        {
          test: /\.vue$/,
          use: [
            'vue-loader',
          ]
        },
        //- vue : npm i -D pug pug-plain-loader
        // {
        //  test: /\.pug$/,
        //  loader: 'pug-plain-loader'
        // },

        // {
        //  /*---------------------------------------*/
        //  //-wow.jsのimportには npm のexports-loaderで解決する
        //  //-e.g.
        //  //-npm i -D exports-loader
        //  //-app.jsには import WOW form 'wowjs/dist/wow.js';
        //  /*---------------------------------------*/
        //  test: require.resolve('wowjs/dist/wow.js'),
        //  loader: 'exports?this.WOW'
        // }
      ]
    },
    plugins: [
      /* --------------------------------------- */
      /* dotenv */
      /* --------------------------------------- */
      new Dotenv(),

      /* --------------------------------------- */
      /* vueloader */
      /* --------------------------------------- */
      // new VueLoaderPlugin(),

      /*---------------------------------------*/
      /* ローンチ コメントイン */
      /*---------------------------------------*/
      // new webpack.DefinePlugin({
      //  'process.env': {
      //    NODE_ENV: '"production"'
      //  }
      // })
    ],

    //- 最適化
    // optimization: {
    //   minimize: true,
    //   minimizer: [
    //     new TerserPlugin({
    //       test: /\.js(\?.*)?$/i,
    //       terserOptions: {
    //         compress: true,
    //         output: {
    //           comments: false,
    //           beautify: false,
    //         }
    //       }

    //     })
    //   ],
    // },
    /*
      use jquery and other
    /*---------------------------------------*/
    // externals: {
    //  "jquery": 'jQuery',
    // }

  },


  /*
    config: css
  /*---------------------------------------*/
  sass: {
    src: src + '/sass/*.sass',
    dest: dest + '/css',
    config: {
      // outputStyle: 'compressed',
      /*
        use foundation
        npm i -D foundation-sites,motion-ui
      /*---------------------------------------*/
      // includePaths: ['node_modules/foundation-sites/scss']
      includePaths: ['node_modules/uikit/src/scss']
    },
    //autoprefixer
    browserlist: [
      'last 3 versions',
      'ie >= 11',
    ],
  },


  /*
    config: browsersync
  /*---------------------------------------*/
  browsersync: {
    //browsersyncのオプション
    option: {
      //立ち上げディレクトリ
      server: {
        baseDir: './dest'
      }
    },
    //監視するファイル対象
    watch: [
      dest + '/**/*.html',
      dest + '/**/*.js',
      dest + '/**/*.css'
    ]
  },

  /*
    config: pug
  /*---------------------------------------*/
  pug: {
    src: [src + '/pug/**/*.pug', '!' + src + '/pug/**/_*.pug', '!' + src + '/pug/_**/*.pug'],
    dest: dest + '/',
    config: {
      pretty: true,
      doctype: 'html'
    }
  },

  /*
    config: images ビルド & minify
    npm i gulp-imagesmin
  /*---------------------------------------*/
  images: {
    src: [dest + '/psd/**/*.jpg', dest + '/psd/**/*.png'],
    dest: dest + '/images',
  },


  /*
    config: watch
  /*---------------------------------------*/
  watch: {
    js: relativeSrcPath + '/js/**',
    sass: relativeSrcPath + '/sass/**',
    pug: relativeSrcPath + '/pug/**',
    vue: relativeSrcPath + '/vue/**',
    // images: dest + '/psd/**',
  }

};



/*
  task: webpack
  @index taskwebpack
/*---------------------------------------*/
gulp.task('webpack', (callback) => {
  try {
    //- task
    gulp.src(config.js.src)
      .pipe(plumber())
      .pipe(webpackstream(config.webpack, webpack))
      .pipe(gulp.dest(config.js.dest));
    //- callback
    callback();
  } catch (err) {
    console.log(err)
  }
});


/*
  task: sass
  @index tasksass
/*---------------------------------------*/
gulp.task('sass', (callback) => {
  try {
    //- task
    gulp.src(config.sass.src)
      .pipe(plumber())
      .pipe(sass(config.sass.config))
      .on('error', function (err) {
        console.log(err.message)
      })
      .pipe(postcss([
        autoprefixer({ browsers: config.sass.browsers }),
        mqpacker
      ]))
      .pipe(gulp.dest(config.sass.dest));
    //- callback
    callback();
  } catch (err) {
    console.log(err)
  }
});


/*
  task: pug
  @index taskpug
/*---------------------------------------*/
gulp.task('pug', (callback) => {
  try {
    //- task
    gulp.src(config.pug.src)
      .pipe(plumber())
      .pipe(pug(config.pug.config))
      //- rename for php
      // .pipe(rename({
      //  extname: ".php"
      // }))
      //- 出力
      .pipe(gulp.dest(config.pug.dest));
    //- callback
    callback();
  } catch (err) {
    console.log(err)
  }
});

/*---------------------------------------*/
const browserSync = require('browser-sync').create()
gulp.task('browser-sync', (callback) => {
  browserSync.init({
    server: {
      baseDir: './dest'
    }
  })
  callback();
})
gulp.task('browser-reload', (callback) => {
  browserSync.reload();
  callback();
})


/*
  task: build
  @index taskbuild
/*---------------------------------------*/
gulp.task('build', (callback) => {
  gulp.series(gulp.parallel('webpack', 'sass', 'pug'))
  callback()
});


/*
  task: watch
  @index taskwatch
/*---------------------------------------*/
gulp.task('watch', (callback) => {
  const watch_js = gulp.watch(config.watch.js, gulp.series('webpack','browser-reload'))
  watch_js.on('change', (path, stats) => {
    console.log(path)
  })
  const watch_sass = gulp.watch(config.watch.sass, gulp.series('sass','browser-reload'));
  watch_sass.on('change', (path, stats) => {
    console.log(path)
  })
  const watch_pug = gulp.watch(config.watch.pug, gulp.series('pug','browser-reload'));
  watch_pug.on('change', (path, stats) => {
    console.log(path)
  })

  const watch_vue = gulp.watch(config.watch.vue, gulp.series('webpack'));
  watch_vue.on('change', (path, stats) => {
    console.log(path)
  })

  callback();
});


/*
  task: default
  @index taskdefault
/*---------------------------------------*/
gulp.task('default', gulp.series('build', 'watch','browser-sync'));
